Les diagrammes sont fait avec StarUML

J'ai décidé de faire 4 entitées: utilisateur, compte, operation et categorie
L'utilisateur et le compte sont liés en "many to many". Opération et catégorie sont liés en "many to one".

Les attributs des classes sont privés ils ne seront pas modifiés par des éléments externes.
La classe BudgetManagement contient les méthodes qui vont gérer l'application, par exemple lier une opération à un compte, modifier le solde d'un compte, voir le solde d'un compte, voir un ou plusieurs compte.
J'ai essayé" de mettre des noms d'attributs et de méthode explicitent dans le contexte d'une gestion de budget
